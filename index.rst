.. Malbar Hackers documentation master file, created by
   sphinx-quickstart on Thu Dec 13 23:31:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Malbar Hackers's documentation!
==========================================

Test bla bla

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Sample code
-----------

.. code-block:: python

    # turtle: simple.png

    forward(100)

.. code-block:: python

    # turtle: rect.png

    forward(100)
    left(90)
    forward(60)
    left(90)
    forward(100)
    left(90)
    forward(60)


.. code-block:: python
    
    # turtle: colors.png

    colors = ['red', 'green', 'blue', 'yellow']

    for c in colors:
        color(c)
        forward(100)
        left(90)


.. code-block:: python

    # turtle: circle.png
    # 18 * 20 = 360 = full circle
    for _ in range(18):
        forward(30)
        left(20)


.. code-block:: python

    # turtle: circle2.png

    steps = 9
    step_size = 50

    for _ in range(steps):
        forward(step_size)
        left(360/steps)


.. code-block:: python

    # turtle: fct.png

    def draw_circle(x, y, steps, step_size):
        up()
        goto(x,y)
        down()
        for _ in range(steps):
            forward(step_size)
            left(360/steps)

    draw_circle(-50,-50,40,20)

    
.. code-block:: python

    # turtle: fct2.png

    def draw_circle(x, y, steps, step_size):
        up()
        goto(x,y)
        down()
        for _ in range(steps):
            forward(step_size)
            left(360/steps)

    for x in range(3):
        for y in range(4):
            draw_circle(50*x-50,50*y-50,20,10)


    
.. code-block:: python

    # turtle: fct3.png

    def draw_circle(x, y, steps, step_size, color):
        up()
        goto(x,y)
        down()
        pencolor(color)
        for _ in range(steps):
            forward(step_size)
            left(360/steps)


    circles = [
        (-10, -10, 20, 5, 'red'),
        (-30, 50, 20, 10, 'blue'),
        (10, 10, 20, 5, 'yellow'),
        (30, -50, 40, 10, 'green') 
    ]

    for x, y, s, size, c in circles:
        draw_circle(x, y, s, size, c)
