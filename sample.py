from turtle import *


def draw_circle(x, y, steps, step_size, color):
    up()
    goto(x,y)
    down()
    pencolor(color)
    for _ in range(steps):
        forward(step_size)
        left(360/steps)


circles = [
    (-10, -10, 20, 5, 'red'),
    (-30, 50, 20, 10, 'blue'),
    (10, 10, 20, 5, 'yellow'),
    (30, -50, 40, 10, 'green') 
]

for x, y, s, size, c in circles:
    draw_circle(x, y, s, size, c)
