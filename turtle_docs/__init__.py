# ...
from docutils.nodes import literal_block, image, Text
from turtle import *
import turtle as t
from PIL import Image

def setup(app):
    app.connect('doctree-resolved', process_nodes)
    return { 'version': '0.1' }


def process_nodes(app, doctree, fromdocname):
    for node in doctree.traverse(literal_block):
        print(node)
        print(dir(node))
        print(node.get('language'))
        print(node.rawsource)

        last = node.rawsource.split('\n')[0]
        if last.startswith('#'):
            name = last.split(':')[-1].strip()

            t.reset()
            t.setup(width=600,height=400)
            exec(node.rawsource)
            t.getscreen().getcanvas().postscript(file='tmp.ps')
            Image.open('tmp.ps').save('_build/html/' + name)
            print('image saved')

            i = image("bla", candidates={'*': name}, uri=name)
            p = node.parent
            idx = p.index(node)
#            print("index:", p.find(node))
#            p.append(i)
            p.insert(idx+1, i)
